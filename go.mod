module gitlab.com/72nd/prfm-ctrl

go 1.13

require (
	github.com/creasty/defaults v1.3.0
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gdamore/tcell v1.3.0
	github.com/lithammer/fuzzysearch v1.1.0
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/rivo/tview v0.0.0-20200219210816-cd38d7432498
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.22.2
	gitlab.com/72nd/prfm-atem v1.2.3
	gitlab.com/72nd/prfm-cuelights v1.2.3
	gitlab.com/72nd/prfm-osc v1.2.3
	gitlab.com/72nd/prfm-player v1.2.3
	gopkg.in/yaml.v2 v2.2.8
)
