package control

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"gitlab.com/72nd/prfm-ctrl/pkg"
)

func (v *View) FileWatcher() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		pkg.Log.Error(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					v.app.QueueUpdateDraw(func() {
						v.message(fmt.Sprintf("«%s» has changed, view updated.", v.cfgPath))
						v.Update()
					})
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				pkg.Log.Error(err)
			}
		}
	}()

	err = watcher.Add(v.watchPath)
	if err != nil {
		pkg.Log.Error(err)
	}
	<-done
}
