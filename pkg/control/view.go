package control

import (
	"fmt"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"gitlab.com/72nd/prfm-ctrl/pkg/data"
	osc "gitlab.com/72nd/prfm-osc/pkg"
	"strings"
	"time"
)

type View struct {
	cfgPath     string
	watchPath   string
	projectName string
	oscHost     string
	oscPort     int
	app         *tview.Application
	tree        *tview.TreeView
	properties  *tview.TextView
	status      *tview.TextView
	currentCue  data.Cue
	retainFocus retainFocus
}

var BackgroundColor = tcell.NewRGBColor(42, 42, 42)
var BorderColor = tcell.NewRGBColor(95, 95, 95)

func NewView(cfgPath string) *View {
	app := tview.NewApplication()
	tree := tview.NewTreeView()
	tree.SetBorder(true).
		SetTitle("Cues").
		SetBackgroundColor(BackgroundColor).
		SetBorderColor(BorderColor)

	properties := tview.NewTextView()
	properties.
		SetDynamicColors(true).
		SetTitle("Properties").
		SetBorder(true).
		SetBackgroundColor(BackgroundColor).
		SetBorderColor(BorderColor)

	status := tview.NewTextView()
	status.SetTextColor(BackgroundColor).SetBackgroundColor(tcell.ColorGreen)

	flex := tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(tree, 0, 1, true).
		AddItem(properties, 7, 1, false).
		AddItem(status, 1, 1, false)

	app.SetRoot(flex, true)
	app.SetFocus(tree)

	cfg := data.OpenConfig(cfgPath)

	view := &View{
		cfgPath:     cfgPath,
		watchPath:   cfg.FolderPath,
		projectName: cfg.Project,
		oscHost:     cfg.OscDestinationHost,
		oscPort:     cfg.OscDestinationPort,
		app:         app,
		tree:        tree,
		properties:  properties,
		status:      status,
		retainFocus: NewRetainFocus(cfg.UseRetainFocus),
	}
	view.app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyInsert {
			app.Stop()
		} else if event.Rune() == ' ' {
			view.cueGo()
		} else if event.Key() == tcell.KeyTab {
			view.toggleItem(tree.GetCurrentNode())
		} else if event.Key() == tcell.KeyCtrlT {
			view.toggleAll()
		} else if event.Rune() == 'S' {
			view.sendCue(cfg.SCommand, 0, 0)
		}
		return event
	})
	tree.SetChangedFunc(view.treeNodeSelected)

	return view
}

func (v *View) Run() {
	v.message("Welcome to prfm-ctrl quit with <Insert>")
	if err := v.app.Run(); err != nil {
		pkg.Log.Error(err)
	}
}

func (v *View) Update() {
	cfg := data.OpenConfig(v.cfgPath)
	root := tview.NewTreeNode(fmt.Sprintf("Cues for project «%s»", v.projectName))
	root.SetSelectable(false)
	root = addChildrenToNodes(root, cfg.Cues)
	v.tree.SetRoot(root).SetCurrentNode(root)
	v.toggleAll()
}

func addChildrenToNodes(node *tview.TreeNode, cues data.Cues) *tview.TreeNode {
	idSpc := cues.IdNameSpaces()
	nameSpc := cues.NameDescSpaces()
	for i := range cues {
		child := tview.NewTreeNode(cues[i].SpacedString(idSpc, nameSpc))
		child.SetReference(cues[i])
		if len(cues[i].SubCues) != 0 {
			child = addChildrenToNodes(child, cues[i].SubCues)
		}
		node.AddChild(child)
	}
	return node
}

func (v *View) treeNodeSelected(node *tview.TreeNode) {
	switch node.GetReference().(type) {
	case data.Cue:
		v.changeProperties(node.GetReference().(data.Cue))
		v.currentCue = node.GetReference().(data.Cue)
	}

}

func (v *View) changeProperties(cue data.Cue) {
	v.properties.SetText(fmt.Sprintf("     [::b]Number:[::-] %s\n"+
		"       [::b]Name:[::-] %s\n"+
		"[::b]Description:[::-] %s\n"+
		"[::b]OSC command:[::-] %s\n" +
		"      [::b]Other:[::-] fade: %dms / wait before: %dms", cue.Id, cue.Name, cue.Description, cue.Command, cue.Fade, cue.WaitBefore))
}

func (v *View) message(msg string) {
	v.status.SetText(msg)
}

func (v *View) cueGo() {
	v.sendCue(v.currentCue.Command, v.currentCue.Fade, v.currentCue.WaitBefore)
	if v.currentCue.SubCues != nil {
		for i := range v.currentCue.SubCues {
			v.sendCue(v.currentCue.SubCues[i].Command, v.currentCue.SubCues[i].Fade, v.currentCue.WaitBefore)
		}
	}

	if v.currentCue.RetainFocus {
		err := v.retainFocus.gainFocus()
		if err != nil {
			v.message(err.Error())
		}
	}

	v.tree.GetCurrentNode().SetColor(tcell.ColorGreen)
	v.message(fmt.Sprintf("Command %s from cue «%s – %s» sent.", v.currentCue.Command, v.currentCue.Id, v.currentCue.Name))
	time.Sleep(100 * time.Millisecond)
	oldNode := v.tree.GetCurrentNode()
	newNode := getNext(v.tree.GetRoot().GetChildren(), v.tree.GetCurrentNode())
	v.tree.SetCurrentNode(newNode)
	v.treeNodeSelected(newNode)
	go func() {
		v.app.QueueUpdateDraw(func() {
			time.Sleep(500 * time.Millisecond)
			oldNode.SetColor(tcell.ColorWhite)
		})
	}()

	if v.currentCue.Command == "todo" {
		v.status.SetText(fmt.Sprintf("Implementation of cue «%s» pending.", v.currentCue.Name))
		v.status.SetBackgroundColor(tcell.ColorOrange)
		go func() {
			v.app.QueueUpdateDraw(func() {
				time.Sleep(1 * time.Second)
				v.status.SetBackgroundColor(tcell.ColorGreen)
			})
		}()
	}
}

func (v *View) sendCue(oscCmd string, intValue, waitBefore int) {
	time.Sleep(time.Duration(waitBefore) * time.Millisecond)
	if oscCmd == "" {
		return
	}
	msg := osc.Message{
		Host:    v.oscHost,
		Port:    v.oscPort,
		Address: oscCmd,
	}

	if intValue != 0 {
		msg.Integers = []int32{int32(intValue)}
	}
	msg.Send()
}

func getNext(items []*tview.TreeNode, current *tview.TreeNode) *tview.TreeNode {
	for i := range items {
		if items[i] == current && i < len(items)-1 {
			return items[i+1]
		}
	}
	return current
}

func (v *View) toggleItem(node *tview.TreeNode) {
	if len(node.GetChildren()) == 0 {
		return
	}
	if node.IsExpanded() {
		node.Collapse()
		node.SetText(fmt.Sprintf("%s (#)", node.GetText()))
	} else {
		node.Expand()
		node.SetText(strings.TrimSuffix(node.GetText(), " (#)"))
	}
}

func (v *View) toggleAll() {
	allCollapsed := true
	for _, item := range v.tree.GetRoot().GetChildren() {
		if item.IsExpanded() {
			allCollapsed = false
			break
		}
	}

	for _, item := range v.tree.GetRoot().GetChildren() {
		if allCollapsed {
			item.Expand()
			item.SetText(strings.TrimSuffix(item.GetText(), " (#)"))
		} else {
			item.Collapse()
			if len(item.GetChildren()) > 0 {
				item.SetText(fmt.Sprintf("%s (#)", item.GetText()))
			}
		}
	}
}
