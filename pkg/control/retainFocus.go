package control

import (
	"fmt"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"os/exec"
	"regexp"
	"time"
)

type retainFocus struct {
	isActive     bool
	ctrlWindowId string
}

func NewRetainFocus(isActive bool) retainFocus {
	var id string
	if isActive {
		id = currentActiveWindow()
	}
	return retainFocus{
		isActive:     isActive,
		ctrlWindowId: id,
	}
}

func currentActiveWindow() string {
	output, err := exec.Command("xprop", "-root").Output()
	if err != nil {
		pkg.Log.Fatal(err)
	}
	rsl := regexp.MustCompile(`_NET_ACTIVE_WINDOW\(WINDOW\): .*#\s(.*)`).FindStringSubmatch(string(output))
	if len(rsl) != 2 {
		pkg.Log.Fatal("unable to identify active window: ", rsl)
	}
	pkg.Log.Debug("current window identified as ", rsl[1])
	return rsl[1]
}

func (f retainFocus) gainFocus() error {
	if !f.isActive {
		return nil
	}
	time.Sleep(500 * time.Millisecond)
	err := exec.Command("wmctrl", "-ia", f.ctrlWindowId).Run()
	if err != nil {
		return fmt.Errorf("error while retain focus with wmctrl: %s", err)
	}
	return nil
}
