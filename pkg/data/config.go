package data

import (
	"github.com/creasty/defaults"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path"
)

type Config struct {
	Project            string   `yaml:"project" default:"Hamlet"`
	OscDestinationHost string   `yaml:"osc-destination-host" default:"localhost"`
	OscDestinationPort int      `yaml:"osc-destination-port" default:"9000"`
	UseRetainFocus     bool     `yaml:"use-retain-focus" default:"false"`
	SCommand           string   `yaml:"s-command" default:"/player/stop-all"`
	AtemConfigs        []string `yaml:"atem-configs" default:"[]"`
	CuelightsConfigs   []string `yaml:"cuelights-configs" default:"[]"`
	PlayerConfigs      []string `yaml:"player-configs" default:"[]"`
	Cues               Cues     `yaml:"cues" default:"[]"`
	FolderPath         string   `yaml:"-"`
	path               string   `yaml:"-"`
}

func NewConfig(path string) *Config {
	cfg := &Config{
		Cues:       []Cue{NewCue()},
		FolderPath: folderPath(path),
		path:       path,
	}
	if err := defaults.Set(cfg); err != nil {
		pkg.Log.Error("default config generation failed: ", err)
	}
	return cfg
}

func OpenConfig(path string) *Config {
	cfg := &Config{
		path: path,
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		pkg.Log.Fatalf("error while reading %s from disk: %s", path, err.Error())
	}
	if err := yaml.Unmarshal(data, cfg); err != nil {
		pkg.Log.Fatal("error while parsing config yaml: ", err)
	}
	pkg.Log.Debugf("config loaded from %s", path)
	return cfg
}

func folderPath(input string) string {
	if path.IsAbs(input) {
		return path.Dir(input)
	}
	wd, err := os.Getwd()
	if err != nil {
		pkg.Log.Fatal(err)
	}
	return path.Join(wd, input)
}

func (c *Config) SaveConfig() {
	data, err := yaml.Marshal(c)
	if err != nil {
		pkg.Log.Error("error while yaml marshal: ", err)
	}
	if err := ioutil.WriteFile(c.path, data, 0644); err != nil {
		pkg.Log.Error("error while write yaml to disk: ", err)
	}
}
