package data

import (
	"fmt"
)

type Addable struct {
	Name                string
	Command             string
	Children            []Addable
}

func (a Addable) ChildrenAsString() string {
	var result string
	if len(a.Children) == 0 {
		return result
	}
	result = a.Children[0].Name
	for i := 1; i < len(a.Children); i++ {
		result = fmt.Sprintf("%s, %s", result, a.Children[i].Name)
	}
	return result
}
