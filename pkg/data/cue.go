package data

import (
	"fmt"
	"github.com/creasty/defaults"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"strings"
)

type Cue struct {
	Id          string `yaml:"id" default:"1"`
	Name        string `yaml:"name" default:"A cue"`
	Description string `yaml:"description" default:"I'm a cue"`
	Command     string `yaml:"command" default:"/path/to/cmd"`
	RetainFocus bool   `yaml:"retain-focus" default:"false"`
	Fade        int    `yaml:"fade" default:"-"`
	WaitBefore  int    `yaml:"wait-before" default:"-"`
	SubCues     Cues   `yaml:"sub-cue" default:"[]"`
	id          int    `yaml:"-"`
}

func NewCue() Cue {
	cue := &Cue{}
	if err := defaults.Set(cue); err != nil {
		pkg.Log.Error("default config generation failed: ", err)
	}
	return *cue
}

func (c Cue) SpacedString(maxIdSpace, maxNameSpace int) string {
	idSpc := strings.Repeat(" ", maxIdSpace-len(c.Id))
	if c.Description != "" {
		maxNameSpace -= len(c.Name)
		maxNameSpace += strings.Count(c.Name, "ö")
		maxNameSpace += strings.Count(c.Name, "ä")
		maxNameSpace += strings.Count(c.Name, "ü")
		return fmt.Sprintf("%s)%s%s%s– %s", c.Id, idSpc, c.Name, strings.Repeat(" ", maxNameSpace), c.Description)
	} else {
		return fmt.Sprintf("%s)%s%s", c.Id, idSpc, c.Name)
	}
}

func (c Cue) String() string {
	if c.Description != "" {
		return fmt.Sprintf("%s) %s – %s", c.Id, c.Name, c.Description)
	} else {
		return fmt.Sprintf("%s) %s", c.Id, c.Name)
	}
}
