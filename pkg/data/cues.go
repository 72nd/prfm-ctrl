package data

type Position int

const (
	Above Position = iota
	Below
	Sub
)

type Cues []Cue

func (c Cues) Index() {
	for i := range c {
		c[i].id = i
		if c[i].SubCues != nil {
			c[i].SubCues.Index()
		}
	}
}

func (c *Cues) AddCue(new, position Cue, pos Position) {
	id := position.id
	cues := *c
	switch pos {
	case Above:
		cues = append(cues[:id], append(Cues{new}, cues[id:]...)...)
		*c = cues
	case Below:
		cues = append(cues[:id+1], append(Cues{new}, cues[id+1:]...)...)
		*c = cues
	case Sub:
		position.SubCues = append(position.SubCues, new)
	}
}

func (c *Cues) AddFirstCue(new Cue) {
	*c = Cues{new}
}

func (c Cues) IdNameSpaces() int {
	spc := 0
	for _, cue := range c {
		if len(cue.Id) > spc {
			spc = len(cue.Id)
		}
	}
	return spc + 1
}

func (c Cues) NameDescSpaces() int {
	spc := 0
	for _, cue := range c {
		if len(cue.Name) > spc {
			spc = len(cue.Name)
		}
	}
	return spc + 1
}
