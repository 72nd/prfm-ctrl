package interactive

import (
	"bufio"
	"fmt"
	"github.com/logrusorgru/aurora"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"os"
	"strings"
)

func inputWithQuit(prompt string) string {
	text := input(fmt.Sprintf("%s (q to quit)", prompt))
	if text == "q" {
		os.Exit(0)
	}
	return text
}

func input(prompt string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(aurora.Bold(fmt.Sprintf("> %s: ", prompt)))
	text, err := reader.ReadString('\n')
	if err != nil {
		pkg.Log.Error(err)
	}
	text = strings.TrimSuffix(text, "\n")
	return text
}

