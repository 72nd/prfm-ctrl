package interactive

import (
	"fmt"
	"github.com/lithammer/fuzzysearch/fuzzy"
	"gitlab.com/72nd/prfm-ctrl/pkg/data"
	"os"
	"strconv"
)

func browseForCommand(adb []data.Addable) string {
	for i := range adb {
		var secondary string
		if adb[i].Command == "" {
			secondary = adb[i].ChildrenAsString()
		} else {
			secondary = adb[i].Command
		}
		fmt.Printf("%d) %s (%s)\n", i+1, adb[i].Name, secondary)
	}

	for {
		text := input("choose element (q to quit, s to searchAddable)")
		if text == "q" {
			os.Exit(0)
		} else if text == "s" {
			return browseForCommand(searchAddable(adb))
		}

		value, err := strconv.Atoi(text)
		if err != nil {
			continue
		}
		if value >= 0 && value <= len(adb) {
			ele := adb[value-1]
			if ele.Children == nil {
				return ele.Command
			} else {
				return browseForCommand(ele.Children)
			}
		}
	}

	return ""
}

func searchAddable(adb []data.Addable) []data.Addable {
	text := inputWithQuit("enter search term")
	var rsl []data.Addable
	for i := range adb {
		text = text[:len(text)-1]
		if fuzzy.Match(text, adb[i].Name) {
			rsl = append(rsl, adb[i])
		}
	}
	return rsl
}
