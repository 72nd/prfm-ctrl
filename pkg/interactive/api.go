package interactive

import (
	"fmt"
	atemUtil "gitlab.com/72nd/prfm-atem/pkg/util"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"gitlab.com/72nd/prfm-ctrl/pkg/data"
	cuelightsUtil "gitlab.com/72nd/prfm-cuelights/pkg/util"
	playerUtil "gitlab.com/72nd/prfm-player/pkg/util"
)

func AtemApi(path string, item int) {
	cfg := data.OpenConfig(path)
	var adb []data.Addable
	for i := range cfg.AtemConfigs {
		adb = append(adb, atemUtil.GetAddable(cfg.AtemConfigs[i])...)
	}
	api(adb, item)
}

func CuelightsApi(path string, item int) {
	cfg := data.OpenConfig(path)
	var adb []data.Addable
	for i := range cfg.AtemConfigs {
		adb = append(adb, cuelightsUtil.GetAddable(cfg.CuelightsConfigs[i])...)
	}
	api(adb, item)
}

func PlayerApi(path string, item int) {
	cfg := data.OpenConfig(path)
	var adb []data.Addable
	for i := range cfg.AtemConfigs {
		adb = append(adb, playerUtil.GetAddable(cfg.PlayerConfigs[i])...)
	}
	api(adb, item)
}

func api(adb []data.Addable, item int) {
	item--
	if item < -1 || item >= len(adb) {
		pkg.Log.Fatalf("given index (%d) is out of range ", item)
	}
	if item != -1 {
		if adb[item].Children == nil {
			pkg.Log.Fatalf("requested element with index %d has no children", item)
		}
		adb = adb[item].Children
	}
	for i := range adb {
		// id, name, command, children
		sub := ""
		if adb[i].Children != nil {
			sub = adb[i].Children[0].Name
			for j := 1; j < len(adb[i].Children); j++ {
				sub = fmt.Sprintf("%s;%s", sub, adb[i].Children[j].Name)
			}
		}
		fmt.Printf("%d|%s|%s|%s\n", i+1, adb[i].Name, adb[i].Command, sub)
	}
}
