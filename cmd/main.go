package main

import (
	"errors"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/72nd/prfm-ctrl/pkg"
	"gitlab.com/72nd/prfm-ctrl/pkg/control"
	"gitlab.com/72nd/prfm-ctrl/pkg/data"
	"gitlab.com/72nd/prfm-ctrl/pkg/interactive"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "prfm-cli"
	app.Usage = "control prfm and other programs via OSC and a watched yaml file"
	app.Action = func(c *cli.Context) error {
		_ = cli.ShowCommandHelp(c, c.Command.Name)
		return nil
	}
	app.Commands = []cli.Command{
		{
			Name:  "api",
			Usage: "interface for other programs",
			Action: func(c *cli.Context) error {
				_ = cli.ShowCommandHelp(c, c.Command.Name)
				return nil
			},
			Subcommands: []cli.Command{
				{
					Name:    "atem",
					Aliases: []string{"a"},
					Flags: []cli.Flag{
						cli.IntFlag{
							Name: "item, i",
						},
					},
					Action: func(c *cli.Context) error {
						if len(c.Args()) == 0 {
							return errors.New("need a path for the new config file")
						}
						interactive.AtemApi(c.Args().First(), c.Int("item"))
						return nil
					},
				},
				{
					Name:    "cuelights",
					Aliases: []string{"c"},
					Flags: []cli.Flag{
						cli.IntFlag{
							Name: "item, i",
						},
					},
					Action: func(c *cli.Context) error {
						if len(c.Args()) == 0 {
							return errors.New("need a path for the new config file")
						}
						interactive.CuelightsApi(c.Args().First(), c.Int("item"))
						return nil
					},
				},
				{
					Name:    "player",
					Aliases: []string{"p"},
					Flags: []cli.Flag{
						cli.IntFlag{
							Name: "item, i",
						},
					},
					Action: func(c *cli.Context) error {
						if len(c.Args()) == 0 {
							return errors.New("need a path for the new config file")
						}
						interactive.PlayerApi(c.Args().First(), c.Int("item"))
						return nil
					},
				},
			},
		},
		{
			Name:    "control",
			Aliases: []string{"ctrl", "c"},
			Usage:   "run the show in a tui interface",
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name: "debug, d",
					Usage: "enable debug mode",
				},
			},
			Action: func(c *cli.Context) error {
				if len(c.Args()) == 0 {
					return errors.New("need a path for the new config file")
				}
				if c.Bool("debug") {
					pkg.Log.Level = logrus.DebugLevel
				}
				view := control.NewView(c.Args().First())
				view.Update()
				go view.FileWatcher()
				view.Run()
				return nil
			},
		},
		{
			Name:    "new",
			Aliases: []string{"n"},
			Usage:   "generate a new show file",
			Action: func(c *cli.Context) error {
				if len(c.Args()) == 0 {
					return errors.New("need a path for the new config file")
				}
				cfg := data.NewConfig(c.Args().First())
				cfg.SaveConfig()
				return nil
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Error(err)
	}
}
