iabbrev prfmc - id: <CR>name: <CR>description: <CR>command: <CR>sub-cue:
iabbrev prfms <CR><TAB>- id: <CR>name: <CR>description: <CR>command: <CR>sub-cue:
nmap <silent> <Leader>ec :call CtrlOsc()<CR>

function! CtrlOsc()
    let category = GetCategory()
    if category == ""
        return
    endif
    let cmd = GetOscCmd(category, 0)
    execute "normal! a" . cmd . "\<ESC>"
endfunction

function! GetCategory()
    echo("1) atem\n2) cuelights\n3) player")
    let result = input("choose category: ")
    redraw
    if result == "1"
        return "atem"
    elseif result == "2"
        return "cuelights"
    elseif result == "3"
        return "player"
    else
        echom("invalid category selection")
        return ""
    endif
endfunction

function! GetOscCmd(category, item)
    let rawResults = system("prfm_ctrl api " . a:category . " -i " . a:item . " " . expand("%"))
    let results = []
    for raw in split(rawResults, "\n")
        let results = results + [split(raw, "|")]
    endfor
    let addable = GetAddable(results)
    if addable[2] == ""
        return GetOscCmd(a:category, addable[0])
    else
        echo addable[2]
        return addable[2]
    endif
endfunction
    
function! GetAddable(results)
    for result in a:results
        if result[2] == ""
            echo result[0] . ") CAT «" . result[1] . "»"
        else
            echo result[0] . ") CMD «" . result[1] . "»" 
        endif
    endfor
    let selection = str2nr(input("Choose addable: "))
    redraw
    if selection < 1 || selection > len(a:results)
        echom "Not a valid choice"
        return
    endif
    return a:results[selection - 1]
endfunction
